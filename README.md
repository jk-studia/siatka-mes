# README #

Projekt na zajęcia "Metoda Elementów Skończonych"

Program tworzący siatkę MES (metody elementów skończonych) 2D, pozwalający na obliczenie rozkładu temperatury w nawierzchni asfaltowej dwuwarstwej.

Warunki:

 - bezwietrzna, bezchmurna letnia noc
 - asfalt nagrzany do wysokiej temperatury
 - chłodzenie odbywa się tylko przez wymianę ciepła między powietrzem a asfaltem oraz podłożem i asfaltem