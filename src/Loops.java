public class Loops {


    private double[][] HGlobal;
    private double[] PGlobal;
    private double[] TGlobal;
    private double[][] H;
    private double[] P;
    private double c;
    private double t;
    private Node[] tmp;
    private Grid grid;
    private GlobalData data;

    public Loops(Grid grid, GlobalData data){
        this.grid = grid;
        this.data = data;
        HGlobal = new double[data.getNN()][data.getNN()];
        PGlobal = new double[data.getNN()];
        TGlobal = new double[data.getNN()];
        H = new double[4][4];
        P = new double[4];
        tmp = new Node[4];
        c = 0.0;
        t = 0.0;
    }

    public void run(){
        loopTime();
    }


    private void loopTime(){
        int iteration = 0;
        for(int i = 0; i < data.getSimulationTime(); i+=data.getSimulationStepTime()) {
               resetGlobal();
               loopElement();
               Gauss gauss = new Gauss(HGlobal, PGlobal, data.getNN());
               TGlobal = gauss.gaussElimination();
               grid.updateTemperature(TGlobal);
               double max = TGlobal[0];
               double min = TGlobal[0];
               for(int j = 1; j < data.getNN(); j++){
                   if(TGlobal[j] > max) max = TGlobal[j];
                   if(TGlobal[j] < min) min = TGlobal[j];
               }
               iteration++;
               show(i+data.getSimulationStepTime(), min, max, data.getAmbientTemperature(), iteration);
               // spadek temperatury o 2/3 stopnia na każdy krok czasowy
               data.setAmbientTemperature(data.getAmbientTemperature()-(2.0/3.0));
           }
    }

    private void loopElement(){
        UniversalElement universalElement;
        for(Element e: grid.getEL()){
            resetLocal();
            for(int i = 0; i < 4; i++) {
                tmp[i] = grid.getND().get(e.getId()[i]);
            }

            universalElement = new UniversalElement(grid, e);

            loopNode(e, universalElement);

            for(int i = 0; i < 4; i++){
                for(int j = 0; j < 4; j++){
                    HGlobal[e.getId()[i]][e.getId()[j]] += H[i][j];
                }
                PGlobal[e.getId()[i]] += P[i];
            }
        }
    }

    private void loopNode(Element e, UniversalElement universalElement){
        for(int i = 0; i < 4; i++) {
            t = 0.0;
            for (int j = 0; j < 4; j++) {
                t += universalElement.getShapeFunctionMatrix()[i][j] * tmp[j].getT();
            }

            // ustalnie danych odpowiedniego materiału w danym punkcie
            double tmpDensity = 0.0;
            double tmpConductivity = 0.0;
            double tmpSpecificHeat = 0.0;
            if (tmp[i].getY() < data.getLength2()) {
                tmpDensity = data.getDensity2();
                tmpConductivity = data.getConductivity2();
                tmpSpecificHeat = data.getSpecificHeat2();
            }
            else if (tmp[i].getY() > data.getLength2()){
                tmpDensity = data.getDensity();
                tmpConductivity = data.getConductivity();
                tmpSpecificHeat = data.getSpecificHeat();
            }
            else if(tmp[0].getY() < data.getLength2()) {
                tmpDensity = data.getDensity2();
                tmpConductivity = data.getConductivity2();
                tmpSpecificHeat = data.getSpecificHeat2();
            }
            else if(tmp[3].getY() > data.getLength2()) {
                tmpDensity = data.getDensity();
                tmpConductivity = data.getConductivity();
                tmpSpecificHeat = data.getSpecificHeat();
            }

            for(int j = 0; j < 4; j++){
                for(int k = 0; k < 4; k++){
                    c = universalElement.getShapeFunctionMatrix()[i][j]*universalElement.getShapeFunctionMatrix()[i][k]*universalElement.getDetJacobians()[i]*tmpSpecificHeat*tmpDensity;
                    H[j][k] += (((universalElement.getdNdX()[i][j]*universalElement.getdNdX()[i][k])+(universalElement.getdNdY()[i][j]*universalElement.getdNdY()[i][k]))*tmpConductivity*universalElement.getDetJacobians()[i])+c/data.getSimulationStepTime();
                    P[j] +=c/data.getSimulationStepTime()*t;
                } }
        }


        double detJArea = 0.0;
        for(int i = 0 ; i < 4; i++){
            if(e.getBoundary()[i]&&(i==1||i==3)){
                if(i==0) detJArea = Math.sqrt(Math.pow(tmp[0].getX()-tmp[3].getX(),2)+Math.pow(tmp[0].getY()-tmp[3].getY(),2))/2.0;
                if(i==1) detJArea = Math.sqrt(Math.pow(tmp[1].getX()-tmp[0].getX(),2)+Math.pow(tmp[1].getY()-tmp[0].getY(),2))/2.0;
                if(i==2) detJArea = Math.sqrt(Math.pow(tmp[2].getX()-tmp[1].getX(),2)+Math.pow(tmp[2].getY()-tmp[1].getY(),2))/2.0;
                if(i==3) detJArea = Math.sqrt(Math.pow(tmp[3].getX()-tmp[2].getX(),2)+Math.pow(tmp[3].getY()-tmp[2].getY(),2))/2.0;

                // ustalnie danych odpowiedniego materiału w danej ścianie
                double aT;
                double tmpAlfa = 0.0;
                if(i==3) {
                    tmpAlfa = data.getAlfa();
                    aT = data.getAmbientTemperature();
                }
                else {
                    tmpAlfa = data.getAlfa2();
                    aT = data.getlTemperature();
                }

                for(int j = 0; j < 2; j++){
                    for(int k = 0; k < 4; k++){
                        for(int l = 0; l < 4; l++){
                            H[k][l] += tmpAlfa*detJArea*universalElement.getNArea()[i][j][k]*universalElement.getNArea()[i][j][l];
                        }
                        P[k] += tmpAlfa*aT*detJArea*universalElement.getNArea()[i][j][k];
                    }
                }
            }
        }
    }

    private void resetLocal(){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                H[i][j] = 0;
            }
        }
        for(int i = 0; i < 4; i++){
            P[i] = 0;
        }
    }

    private void resetGlobal(){
        for(int i = 0; i < data.getNN(); i++){
            for(int j = 0; j < data.getNN(); j++){
                HGlobal[i][j] = 0;
            }
        }
        for(int i = 0; i < data.getNN(); i++){
            PGlobal[i] = 0;
            TGlobal[i] = 0;
        }
    }

    public void show(int time, double min, double max, double temp, int iteration){
        System.out.println("\n\n===============================");
        System.out.println("Iteration: " + iteration);
        //System.out.println("Time: " + time);
        System.out.println("Global matrix H");
        System.out.println("======================================");
        for(int i = 0; i < data.getNN(); i++){
            for(int j = 0; j < data.getNN(); j++){
                System.out.print(HGlobal[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println("\nGlobal vector P");
        System.out.println("======================================");
        for(int i = 0; i < data.getNN(); i++){
            System.out.print(PGlobal[i]+" ");
        }
        System.out.println("\n\nGlobal vector T");
        System.out.println("======================================");
        for(int i = 0; i < data.getNN(); i++){
            System.out.println(i+": "+TGlobal[i]);
        }

        System.out.println("\nMin: "+min);
        System.out.println("Max: "+max);
        System.out.println("\nAmbient Temperature:"+temp);

    }

    public double[][] getHGlobal() {
        return HGlobal;
    }


    public double[] getPGlobal() {
        return PGlobal;
    }

    public double[] getTGlobal() {
        return TGlobal;
    }

    public double[][] getH() {
        return H;
    }

    public double[] getP() {
        return P;
    }
}