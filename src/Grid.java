import java.util.ArrayList;
import java.util.List;

public class Grid {
    /*
    *
    * Legenda:
    *
    * ND - lista wszystkich wezlow w siatce
    * EL - lista wszystkich elementow w siatce
    *
    * */

    private int nN;
    private int nE;
    private List<Node> ND;
    private List<Element> EL;
    private double tmpX;
    private double tmpY;
    public Grid(GlobalData data){
        this.nN = data.getNN();
        this.nE = data.getNE();

        ND = new ArrayList<>(nN);
        EL = new ArrayList<>(nE);
        tmpX = data.getB()/(data.getNB()-1);
        tmpY = data.getH()/((data.getNH()-1));
        boolean status;
        for(int i = 0; i < data.getNB(); i++){
            for(int j = 0; j < data.getNH(); j++){
                if((i == 0 || j == 0) || i==data.getNB()-1 || j==data.getNH()-1) status = true;
                else status = false;
                ND.add(new Node(i*tmpX,j*tmpY,status, data.getInitialTemperature()));

            }
        }

        int nodeID = 0;
        double l = 0.0;
        for(int i = 0, j =+ 1; i < data.getNE(); i++, j++){
            EL.add(new Element(i));
            EL.get(i).setID(nodeID,nodeID+data.getNH(),nodeID+data.getNH()+1,nodeID+1, this);

            if(j%(data.getNH()-1)==0) {
                nodeID += 2;
            }
            else
                nodeID++;

        }
    }

    public void updateTemperature(double[] T){
        for(Element e: this.getEL()){
            for(int i = 0; i < 4; i++){
                ND.get(e.getId()[i]).setT(T[e.getId()[i]]);
            }
        }
    }

    public List<Node> getND() {
        return ND;
    }

    public List<Element> getEL() {
        return EL;
    }

    public double getTmpX(){ return tmpX; }
    public double getTmpY(){ return tmpY; }

}
