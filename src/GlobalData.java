import java.io.*;

public class GlobalData {
    /*
    *
    * Legenda:
    *
    * H - odleglosc wezlow pionowo }
    *                              } dlugosci bokow elementu
    * B - odleglosc wezlow poziomo }
    *
    * nH - liczba wezlow pionowo
    * nB - liczba wezlow poziomo
    * nN - liczba wezlow w siatce
    * nE - liczba elementow w siatce
    *
    * */
    private double H,B;                 // wysokość i szerokość sitatki
    private int nH,nB,nN,nE;            // liczby węzłów siatki w wysokości i szerokości, całkowita liczba węzłów, liczba elementów w siatce
    private double initialTemperature;  // temperatura początkowa
    private int simulationTime;         // czas trwania symulacji
    private int simulationStepTime;     // krok czasowy
    private double ambientTemperature;  // temperatura otoczenia
    private double alfa;                // współczynnik alfa konwekcji asfaltu lanego
    private double alfa2;                // współczynnik alfa konwekcji asfaltobetonu
    private double specificHeat;        // pojemność cieplna asfaltu lanego
    private double conductivity;        // Współczynnik przewodzenia ciepła asfaltu lanego
    private double density;             // gęstość asfaltu lanego
    private double specificHeat2;       // pojemność cieplna asfaltobetonu
    private double conductivity2;       // Współczynnik przewodzenia ciepła asfaltobetonu
    private double density2;            // gęstość asfaltobetonu
    private double length;
    private double length2;
    private double lTemperature;
    private File plik;
    private BufferedReader reader;
    private String line;

    public GlobalData(){
        plik = new File("data.txt");
        try {
            reader = new BufferedReader(new FileReader(plik));
        }catch(FileNotFoundException e){
            System.out.println("Error: Nie znaleziono pliku z danymi");
            e.printStackTrace();
        }

        try {
            while((line = reader.readLine())!= null) {
                String[] parts = line.split(":");
                if(parts[0].equals("H")) H = Double.parseDouble(parts[1]);
                if(parts[0].equals("B")) B = Double.parseDouble(parts[1]);
                if(parts[0].equals("nH")) nH = Integer.parseInt(parts[1]);
                if(parts[0].equals("nB")) nB = Integer.parseInt(parts[1]);
                if(parts[0].equals("initial temperature")) initialTemperature = Double.parseDouble(parts[1]);
                if(parts[0].equals("simulation time")) simulationTime = Integer.parseInt(parts[1]);
                if(parts[0].equals("simulation step time")) simulationStepTime = Integer.parseInt(parts[1]);
                if(parts[0].equals("ambient temperature")) ambientTemperature = Double.parseDouble(parts[1]);
                if(parts[0].equals("alfa")) alfa = Double.parseDouble(parts[1]);
                if(parts[0].equals("specific heat")) specificHeat = Double.parseDouble(parts[1]);
                if(parts[0].equals("conductivity")) conductivity = Double.parseDouble(parts[1]);
                if(parts[0].equals("density")) density = Double.parseDouble(parts[1]);
                if(parts[0].equals("length")) length = Double.parseDouble(parts[1]);
                if(parts[0].equals("specific heat 2")) specificHeat2 = Double.parseDouble(parts[1]);
                if(parts[0].equals("conductivity 2")) conductivity2 = Double.parseDouble(parts[1]);
                if(parts[0].equals("density 2")) density2 = Double.parseDouble(parts[1]);
                if(parts[0].equals("alfa 2")) alfa2 = Double.parseDouble(parts[1]);
                if(parts[0].equals("length 2")) length2 = Double.parseDouble(parts[1]);
                if(parts[0].equals("temperature under asphalt")) lTemperature = Double.parseDouble(parts[1]);
            }
            double asr1 = conductivity/(specificHeat*density);
            double tmp1 = Math.pow(B/nB,2)/(0.5*asr1);
            double asr2 = conductivity2/(specificHeat2*density2);
            double tmp2 = Math.pow(B/nB,2)/(0.5*asr2);
            double tmp3 = (tmp1+tmp2)/2.0;
            simulationStepTime = (int)tmp3;
        }catch(IOException e){
            System.out.println("Error: reading line");
        }
        nN = nH*nB;
        nE = (nB-1)*(nH-1);
    }


    public double getB() {
        return B;
    }

    public double getH() {
        return H;
    }

    public int getNH() {
        return nH;
    }

    public int getNB() {
        return nB;
    }

    public int getNN() {
        return nN;
    }
    public int getNE(){
        return nE;
    }


    @Override
    public String toString() {
        return "Dane:"+"\nH = "+ H+"\nB = " + B+"\nnH = " + nH+"\nnB = " + nB+"\nnN = " + nN+"\nnE = " + nE+"\ninitialTemperature = "+initialTemperature+"\nsimulationTime = "+simulationTime+"\nsimulationStepTime = "+simulationStepTime+"\nambientTemperature = "+ambientTemperature+"\nalfa 1= "+alfa+"\nspecificHeat 1= "+specificHeat+"\nconductivity 1= "+conductivity+"\ndensity 1= "+density+"\n"+"\nalfa 2= "+alfa2+"\nspecificHeat 2= "+specificHeat2+"\nconductivity 2= "+conductivity2+"\ndensity 2= "+density2+"\n";
    }

    public double getDensity() {
        return density;
    }

    public double getConductivity() {
        return conductivity;
    }

    public double getSpecificHeat() {
        return specificHeat;
    }

    public double getAlfa() {
        return alfa;
    }

    public double getAmbientTemperature() {
        return ambientTemperature;
    }

    public int getSimulationStepTime() {
        return simulationStepTime;
    }

    public int getSimulationTime() {
        return simulationTime;
    }

    public double getInitialTemperature() {
        return initialTemperature;
    }

    public double getlTemperature() {
        return lTemperature;
    }
    public void setAmbientTemperature(double ambientTemperature){
        this.ambientTemperature = ambientTemperature;
    }

    public double getLength() {
        return length;
    }

    public double getLength2() {
        return length2;
    }

    public double getAlfa2() {
        return alfa2;
    }

    public double getSpecificHeat2() {
        return specificHeat2;
    }

    public double getConductivity2() {
        return conductivity2;
    }

    public double getDensity2() {
        return density2;
    }
}
