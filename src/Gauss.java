public class Gauss {

    private static final double EPSILON = 1e-10;

    private double[][] matrixH;
    private double[] vectorP;
    private int n;
    private double[][] tmp;

    public Gauss(double[][] matrixH, double[] vectorP, int n){
        this.matrixH = matrixH;
        this.vectorP = vectorP;
        this.n = n;
        tmp = new double[n][n+1];

        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                tmp[i][j] = matrixH[i][j];
            }
            tmp[i][n] = vectorP[i];
        }


    }

    public double[] gaussElimination(){
        for (int p = 0; p < n; p++) {

            int max = p;
            for (int i = p + 1; i < n; i++) {
                if (Math.abs(tmp[i][p]) > Math.abs(tmp[max][p])) {
                    max = i;
                }
            }
            double[] tmp1 = tmp[p];
            tmp[p] = tmp[max];
            tmp[max] = tmp1;

            double t =  vectorP[p];
            vectorP[p] =  vectorP[max];
            vectorP[max] = t;

            if (Math.abs(tmp[p][p]) <= EPSILON) {
                throw new ArithmeticException("Matrix is singular or nearly singular");
            }

            for (int i = p + 1; i < n; i++) {
                double alpha = tmp[i][p] / tmp[p][p];
                vectorP[i] -= alpha *  vectorP[p];
                for (int j = p; j < n; j++) {
                    tmp[i][j] -= alpha * tmp[p][j];
                }
            }
        }

        double[] x = new double[n];
        for (int i = n - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < n; j++) {
                sum += tmp[i][j] * x[j];
            }
            x[i] = ( vectorP[i] - sum) / tmp[i][i];
        }
        return x;
    }
}