
public class Main {

    public static void main(String[] args){
        GlobalData data = new GlobalData();
        System.out.println(data);
        Grid grid = new Grid(data);


        Loops loops = new Loops(grid, data);
        loops.run();
    }
}