public class UniversalElement {
    private double[][] derivativeShapeFunctionMatrixKsi;
    private double[][] derivativeShapeFunctionMatrixEta;
    private double[][] shapeFunctionMatrix;
    private double[][] KsiEtaMatrix = {{-1.0/(Math.sqrt(3.0)), -1.0/(Math.sqrt(3.0))}, {1.0/(Math.sqrt(3.0)), -1.0/(Math.sqrt(3.0))}, {1.0/(Math.sqrt(3.0)), 1.0/(Math.sqrt(3.0))}, {-1.0/(Math.sqrt(3.0)), 1.0/(Math.sqrt(3.0))}};
    private double[][][] jacobians;
    private double[] detJacobians;
    private double[][][] reciprocalJacobians;
    private double[][] dNdX;
    private double[][] dNdY;
    private double[][][] NArea;

    public UniversalElement(Grid grid, Element element){
        derivativeShapeFunctionMatrixKsi = new double[4][4];
        derivativeShapeFunctionMatrixEta = new double[4][4];
        shapeFunctionMatrix = new double[4][4];
        jacobians = new double[4][2][2];
        reciprocalJacobians = new double[4][2][2];
        detJacobians = new double[4];
        dNdX = new double[4][4];
        dNdY = new double[4][4];
        NArea = new double[4][2][4];
        fillDerivativeShapeFunctionMatrixXi();
        fillDerivativeShapeFunctionMatrixEta();
        fillShapeFunctionMatrix();
        double x1 = grid.getND().get(element.getId()[0]).getX();
        double y1 = grid.getND().get(element.getId()[0]).getY();
        double x2 = grid.getND().get(element.getId()[1]).getX();
        double y2 = grid.getND().get(element.getId()[1]).getY();
        double x3 = grid.getND().get(element.getId()[2]).getX();
        double y3 = grid.getND().get(element.getId()[2]).getY();
        double x4 = grid.getND().get(element.getId()[3]).getX();
        double y4 = grid.getND().get(element.getId()[3]).getY();
        countJacobians(x1,y1,x2,y2,x3,y3,x4,y4);
        countDetJacobians();
        countDNDXandDNDY();
        fillNArea();
    }


    private void fillDerivativeShapeFunctionMatrixXi(){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                if(j==0)derivativeShapeFunctionMatrixKsi[i][j]=derivativeShapeFunctionN1Ksi(KsiEtaMatrix[i][1]);
                if(j==1)derivativeShapeFunctionMatrixKsi[i][j]=derivativeShapeFunctionN2Ksi(KsiEtaMatrix[i][1]);
                if(j==2)derivativeShapeFunctionMatrixKsi[i][j]=derivativeShapeFunctionN3Ksi(KsiEtaMatrix[i][1]);
                if(j==3)derivativeShapeFunctionMatrixKsi[i][j]=derivativeShapeFunctionN4Ksi(KsiEtaMatrix[i][1]);
            }
        }

    }

    private void fillDerivativeShapeFunctionMatrixEta(){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                if(j==0)derivativeShapeFunctionMatrixEta[i][j]=derivativeShapeFunctionN1Eta(KsiEtaMatrix[i][0]);
                if(j==1)derivativeShapeFunctionMatrixEta[i][j]=derivativeShapeFunctionN2Eta(KsiEtaMatrix[i][0]);
                if(j==2)derivativeShapeFunctionMatrixEta[i][j]=derivativeShapeFunctionN3Eta(KsiEtaMatrix[i][0]);
                if(j==3)derivativeShapeFunctionMatrixEta[i][j]=derivativeShapeFunctionN4Eta(KsiEtaMatrix[i][0]);
            }
        }
    }

    private void fillShapeFunctionMatrix(){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                if(j==0)shapeFunctionMatrix[i][j]=shapeFunctionN1(KsiEtaMatrix[i][0],KsiEtaMatrix[i][1]);
                if(j==1)shapeFunctionMatrix[i][j]=shapeFunctionN2(KsiEtaMatrix[i][0],KsiEtaMatrix[i][1]);
                if(j==2)shapeFunctionMatrix[i][j]=shapeFunctionN3(KsiEtaMatrix[i][0],KsiEtaMatrix[i][1]);
                if(j==3)shapeFunctionMatrix[i][j]=shapeFunctionN4(KsiEtaMatrix[i][0],KsiEtaMatrix[i][1]);
            }
        }

    }

    private void countJacobians(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4){
        double tmp;
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                if(j==0){
                    tmp = derivativeShapeFunctionMatrixKsi[i][0]*x1+derivativeShapeFunctionMatrixKsi[i][1]*x2+derivativeShapeFunctionMatrixKsi[i][2]*x3+derivativeShapeFunctionMatrixKsi[i][3]*x4;
                    jacobians[i][0][0] = tmp;
                    reciprocalJacobians[i][1][1] = tmp;
                }
                if(j==1){
                    tmp = derivativeShapeFunctionMatrixKsi[i][0]*y1+derivativeShapeFunctionMatrixKsi[i][1]*y2+derivativeShapeFunctionMatrixKsi[i][2]*y3+derivativeShapeFunctionMatrixKsi[i][3]*y4;
                    jacobians[i][0][1] = tmp;
                    reciprocalJacobians[i][0][1] = -tmp;
                }
                if(j==2){
                    tmp = derivativeShapeFunctionMatrixEta[i][0]*x1+derivativeShapeFunctionMatrixEta[i][1]*x2+derivativeShapeFunctionMatrixEta[i][2]*x3+derivativeShapeFunctionMatrixEta[i][3]*x4;
                    jacobians[i][1][0] = tmp;
                    reciprocalJacobians[i][1][0] = -tmp;
                }
                if(j==3){
                    tmp = derivativeShapeFunctionMatrixEta[i][0]*y1+derivativeShapeFunctionMatrixEta[i][1]*y2+derivativeShapeFunctionMatrixEta[i][2]*y3+derivativeShapeFunctionMatrixEta[i][3]*y4;
                    jacobians[i][1][1] = tmp;
                    reciprocalJacobians[i][0][0] = tmp;
                }
            }
        }
    }

    private void countDetJacobians(){
        for(int i = 0; i < 4; i++){
            detJacobians[i] = (jacobians[i][0][0]*jacobians[i][1][1])-(jacobians[i][0][1]*jacobians[i][1][0]);
        }
    }

    private void countDNDXandDNDY(){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                dNdX[i][j]=(reciprocalJacobians[i][0][0]*derivativeShapeFunctionMatrixKsi[i][j]+reciprocalJacobians[i][0][1]*derivativeShapeFunctionMatrixEta[i][j])/detJacobians[i];
                dNdY[i][j]=(reciprocalJacobians[i][1][0]*derivativeShapeFunctionMatrixKsi[i][j]+reciprocalJacobians[i][1][1]*derivativeShapeFunctionMatrixEta[i][j])/detJacobians[i];
            }
        }
    }

    public void fillNArea(){

        NArea[0][0][0] = shapeFunctionN1(-1.0, KsiEtaMatrix[3][1]);
        NArea[0][0][1] = shapeFunctionN2(-1.0, KsiEtaMatrix[3][1]);
        NArea[0][0][2] = shapeFunctionN3(-1.0, KsiEtaMatrix[3][1]);
        NArea[0][0][3] = shapeFunctionN4(-1.0, KsiEtaMatrix[3][1]);

        NArea[0][1][0] = shapeFunctionN1(-1.0, KsiEtaMatrix[0][1]);
        NArea[0][1][1] = shapeFunctionN2(-1.0, KsiEtaMatrix[0][1]);
        NArea[0][1][2] = shapeFunctionN3(-1.0, KsiEtaMatrix[0][1]);
        NArea[0][1][3] = shapeFunctionN4(-1.0, KsiEtaMatrix[0][1]);

        NArea[1][0][0] = shapeFunctionN1(KsiEtaMatrix[0][0],-1.0);
        NArea[1][0][1] = shapeFunctionN2(KsiEtaMatrix[0][0],-1.0);
        NArea[1][0][2] = shapeFunctionN3(KsiEtaMatrix[0][0],-1.0);
        NArea[1][0][3] = shapeFunctionN4(KsiEtaMatrix[0][0],-1.0);

        NArea[1][1][0] = shapeFunctionN1(KsiEtaMatrix[1][0],-1.0);
        NArea[1][1][1] = shapeFunctionN2(KsiEtaMatrix[1][0],-1.0);
        NArea[1][1][2] = shapeFunctionN3(KsiEtaMatrix[1][0],-1.0);
        NArea[1][1][3] = shapeFunctionN4(KsiEtaMatrix[1][0],-1.0);


        NArea[2][0][0] = shapeFunctionN1(1.0, KsiEtaMatrix[1][1]);
        NArea[2][0][1] = shapeFunctionN2(1.0, KsiEtaMatrix[1][1]);
        NArea[2][0][2] = shapeFunctionN3(1.0, KsiEtaMatrix[1][1]);
        NArea[2][0][3] = shapeFunctionN4(1.0, KsiEtaMatrix[1][1]);

        NArea[2][1][0] = shapeFunctionN1(1.0, KsiEtaMatrix[2][1]);
        NArea[2][1][1] = shapeFunctionN2(1.0, KsiEtaMatrix[2][1]);
        NArea[2][1][2] = shapeFunctionN3(1.0, KsiEtaMatrix[2][1]);
        NArea[2][1][3] = shapeFunctionN4(1.0, KsiEtaMatrix[2][1]);

        NArea[3][0][0] = shapeFunctionN1(KsiEtaMatrix[2][0],1.0);
        NArea[3][0][1] = shapeFunctionN2(KsiEtaMatrix[2][0],1.0);
        NArea[3][0][2] = shapeFunctionN3(KsiEtaMatrix[2][0],1.0);
        NArea[3][0][3] = shapeFunctionN4(KsiEtaMatrix[2][0],1.0);

        NArea[3][1][0] = shapeFunctionN1(KsiEtaMatrix[3][0],1.0);
        NArea[3][1][1] = shapeFunctionN2(KsiEtaMatrix[3][0],1.0);
        NArea[3][1][2] = shapeFunctionN3(KsiEtaMatrix[3][0],1.0);
        NArea[3][1][3] = shapeFunctionN4(KsiEtaMatrix[3][0],1.0);

    }


    // ================================================================ wzory funkcji kształtu N1, N2, N3, N4
    private double shapeFunctionN1(double ksi, double eta){
        return 0.25*(1-ksi)*(1-eta);
    }
    private double shapeFunctionN2(double ksi, double eta){
        return 0.25*(1+ksi)*(1-eta);
    }
    private double shapeFunctionN3(double ksi, double eta){
        return 0.25*(1+ksi)*(1+eta);
    }
    private double shapeFunctionN4(double ksi, double eta){
        return 0.25*(1-ksi)*(1+eta);
    }

    // ================================================================ wzory pochodnych funkcji kształtu N1, N2, N3, N4 po xi
    private double  derivativeShapeFunctionN1Ksi(double eta){
        return -0.25*(1-eta);
    }
    private double  derivativeShapeFunctionN2Ksi(double eta){
        return 0.25*(1-eta);
    }
    private double  derivativeShapeFunctionN3Ksi(double eta){
        return 0.25*(1+eta);
    }
    private double  derivativeShapeFunctionN4Ksi(double eta){
        return -0.25*(1+eta);
    }

    // ================================================================ wzory pochodnych funkcji kształtu N1, N2, N3, N4 po eta
    private double  derivativeShapeFunctionN1Eta(double ksi){
        return -0.25*(1-ksi);
    }
    private double  derivativeShapeFunctionN2Eta(double ksi){
        return -0.25*(1+ksi);
    }
    private double  derivativeShapeFunctionN3Eta(double ksi){
        return 0.25*(1+ksi);
    }
    private double  derivativeShapeFunctionN4Eta(double ksi){
        return 0.25*(1-ksi);
    }

    public double[][] getdNdX() {
        return dNdX;
    }

    public double[][] getdNdY() {
        return dNdY;
    }

    public double[][][] getJacobians(){ return jacobians; }
    public double[] getDetJacobians() { return detJacobians; }
    public double[][][] getReciprocalJacobians() { return reciprocalJacobians; }
    public double[][] getDerivativeShapeFunctionMatrixKsi() {
        return derivativeShapeFunctionMatrixKsi;
    }
    public double[][] getDerivativeShapeFunctionMatrixEta() {
        return derivativeShapeFunctionMatrixEta;
    }
    public double[][] getShapeFunctionMatrix() {
        return shapeFunctionMatrix;
    }
    public double[][][] getNArea(){ return NArea; }
}