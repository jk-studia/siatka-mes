public class Node {
    /*
    * Legenda:
    *
    * x,y - wspolrzedne wezla
    * t - temperatura
    * status - status wezla: 1/true - wezel brzegowy, 0/false - wezel wewnetrzny
    *
    * */

    private double t;
    private double x,y;
    private boolean status;

    public Node(double x, double y, boolean status, double t){
        this.x = x;
        this.y = y;
        this.status = status;
        this.t = t;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getT() {
        return t;
    }

    public void setT(double t){
        this.t = t;
    }

    public boolean getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "[x,y]=["+x+","+y+"], status = " + status+", temperature: "+t;
    }
}
