public class Element {

    /*
    * Legenda:
    *
    * id - id wezlow elementu
    * value - wartosc elementu
    * */

    private int[] id;
    private int value;
    private boolean[] boundary; // indeksy 0 - ściana lewa, 1 - ściana dolna, 2 - ściana prawa, 3 - ściana górna

    public Element(int value){
        this.value = value;
        id = new int[4];
        boundary = new boolean[4];
    }

    public void setID(int ID1, int ID2, int ID3, int ID4, Grid grid){
        id[0] = ID1;
        id[1] = ID2;
        id[2] = ID3;
        id[3] = ID4;
        checkBoundary(grid);
    }

    private void checkBoundary(Grid grid){
        if(grid.getND().get(id[3]).getStatus()&&grid.getND().get(id[0]).getStatus()){
            boundary[0] = true;
        }
        else {
            boundary[0] = false;
        }
        if(grid.getND().get(id[0]).getStatus()&&grid.getND().get(id[1]).getStatus()){
            boundary[1] = true;
        }
        else {
            boundary[1] = false;
        }

        if(grid.getND().get(id[1]).getStatus()&&grid.getND().get(id[2]).getStatus()){
            boundary[2] = true;
        }
        else {
            boundary[2] = false;
        }

        if(grid.getND().get(id[2]).getStatus()&&grid.getND().get(id[3]).getStatus()){
            boundary[3] = true;
        }
        else {
            boundary[3] = false;
        }
    }

    public boolean[] getBoundary(){
        return boundary;
    }

    public int getValue() {
        return value;
    }

    public int[] getId() {
        return id;
    }



    @Override
    public String toString() {
        return "Element value = "+value+", ID1 = "+id[0]+", ID2 = "+id[1]+", ID3 = "+id[2]+", ID4 = "+id[3];
    }
}